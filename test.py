from mininet.topo import Topo

class MyTopo(Topo):
    def __init__(self):

        Topo.__init__(self)

        host_1=self.addHost("h1")
        host_2=self.addHost("h2")
        switch_1=self.addSwitch("s1")
        switch_2=self.addSwitch("s2")
	switch_3=self.addSwitch("s3")
        self.addLink(host_1,switch_1)
	self.addLink(switch_1,switch_3)
	self.addLink(switch_3,switch_2)
        self.addLink(host_2,switch_2)
        self.addLink(switch_1,switch_2)

topos={"mytopo":(lambda:MyTopo())}